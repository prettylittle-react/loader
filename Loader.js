import React from 'react';
import PropTypes from 'prop-types';

import {getModifiers} from 'libs/component';

import './Loader.scss';

/**
 * Loader
 * @description [Description]
 * @example
  <div id="Loader"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Loader, {
    	title : 'Example Loader'
    }), document.getElementById("Loader"));
  </script>
 */
class Loader extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'loader';
	}

	componentDidMount() {
		setTimeout(() => {
			if (this.component) {
				this.component.classList.add('active');
			}
		}, 100);
	}

	render() {
		const {theme} = this.props;

		const className = getModifiers(this.baseClass, [theme]);

		return (
			<span className={className} ref={component => (this.component = component)}>
				<span />
			</span>
		);
	}
}

Loader.defaultProps = {
	theme: null,
	children: null
};

Loader.propTypes = {
	theme: PropTypes.string,
	children: PropTypes.node
};

export default Loader;
